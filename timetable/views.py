from django.shortcuts import render
from .models import Schedule


# Create your views here.




def home(request):

    # times = Schedule.objects.all()
    # print(times)

    return render(request, 'home.html')

def search(request):

    location = request.GET['location']
    location = str(location.upper())

    times = Schedule.objects.filter(area__area=location)
    if not times:
        found = False
        return render(request, 'result.html', {'found':found})
    else:
        print(location)
        return render(request, 'result.html', {'times' : times, 'location':location})

